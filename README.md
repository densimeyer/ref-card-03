# Architecture Ref. Card 03

Spring Boot Application mit MariaDB Database

Denis Meyer

## Inbetriebnahme auf eigenem Computer

1. Projekt herunterladen / Clonen
2. .env.example kopieren
3. .env.example kopie zu .env umbennen
4. Terminal im Projektordner öffnen
5. docker compose -f docker-compose.yml up -d eingeben.
6. Im Browser ist die App nun unter der URL http://localhost:8080 erreichbar.

## Umgebungsvariabeln

| Variabel | Beispielvalue |
|----------|----------|
| AWS_ACCESS_KEY_ID    | ASIAVWQ22QTHKNERDPONB   |
| AWS_DEFAULT_REGION   | us-west-5   | 
| AWS_SECRET_ACCESS_KEY   | sVUfD8S+zvIBB/mhNhjRyStG1A/xX6GVedOk8   |
| AWS_SESSION_TOKEN    | FwoGZXIvYXdzEOv//////////wEaDFrq0BTw4si3DMPtmyK8ATopI0yMqxka6oFQfJmgKOEmYDcHj2/0r0RHOefoMVhN9ACU7wr8FP+Oz2KPW0jXkjz6HTjPT6RePcVA4MYmc9M5o07EVypF+zsaVJHGEPN9syKxMBB0JA4368WJxAk23uVp2lZxrxMKhDktuNwftD+aoIUkG7yHRPRsYMLH0oF97ksxag3pSFq3DPAwoo+obYklUZgjBaG+onJavKEiDmX6H0aXsCnCoyq8Q1xFKz50wxXmoYd1Apc9qfSaKOHfvKMGMi0mQFZRSZhkCrZiNrwzC+L/SPfsva1QIVBBBVCDDTIOMWQYYCT568   | 
| CI_AWS_ECR_REGISTRY   | 39197453436796656.dkr.ecr.us-east-1.amazonaws.com   |
| CI_AWS_ECR_REPOSITORY_NAME   | meyer-refcard03   |
| CD_AWS_ECS_SERVICE | meyer-refcard03-service |
| CD_AWS_ECS_CLUSTER | meyer-refcard03-cluster |

## Wichtiges für AWS 

Stellen sie sicher, dass sie eine Elastic Container Registry (ECR), einen Elastic Container Service (ECS) und einen RDS erstellt haben.


